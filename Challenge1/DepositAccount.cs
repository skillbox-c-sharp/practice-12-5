﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    internal class DepositAccount : Account
    {
        public DepositAccount(int clientId) : base(clientId) 
        {
            IsOpen = false;
        }

        public DepositAccount(int clientId, int accountNumber, double balance, bool isOpen)
            : base(clientId, accountNumber, balance, isOpen) { }

        public override double CheckAccumulation(float interestRate)
        {
            if (IsOpen)
            {
                double tempBalance = Balance;
                for (int i = 0; i < 12; i++)
                {
                    tempBalance += tempBalance * interestRate / 12;
                }
                return tempBalance;
            }
            else
            {
                return 0;
            }
        }
    }
}
