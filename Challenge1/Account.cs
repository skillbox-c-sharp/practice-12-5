﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    internal class Account
    {
        private static int currentAccountNumber;

        public int AccountNumber { get; private set; }
        public int ClientId { get; private set; }
        public double Balance { get; private set; }
        public bool IsOpen { get; protected set; }
    
        public Account(int clientId)
        {
            ClientId = clientId;
            AccountNumber = currentAccountNumber++;
            Balance = 0;
            IsOpen = true;
        }
        public Account(int clientId, int accountNumber, double balance, bool isOpen)
        {
            ClientId = clientId;
            AccountNumber = accountNumber;
            Balance = balance;
            IsOpen = isOpen;
        }

        static Account()
        {
            currentAccountNumber = 100000;
        }

        public void CloseAccount()
        {
            if (IsOpen)
            {
                IsOpen = false;
                Balance = 0;
            }
        }

        public void OpenAccount()
        {
            if (!IsOpen)
            {
                IsOpen = true;
            }
            
        }

        public void TopUpAccount(double value)
        {
            if (IsOpen)
            {
                Balance += value;
            }
        }

        public void WithdrawAccount(double value)
        {
            if (IsOpen && Balance >= value)
            {
                Balance -= value;
            }
        }

        public virtual double CheckAccumulation(float interestRate)
        {
            if (IsOpen)
            {
                return Balance + Balance * interestRate;
            }
            else
            {
                return 0;
            }
        }

        public void TransferAccount(Account accTo, double value)
        {
            if (IsOpen && Balance >= value && accTo.IsOpen)
            {
                accTo.Balance += value;
                Balance -= value;
            }
        }
    }
}
