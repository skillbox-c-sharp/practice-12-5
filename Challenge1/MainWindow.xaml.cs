﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Challenge1
{
    public partial class MainWindow : Window
    {
        List<Client> clients;
        int clientId;
        bool isDeposit;

        public MainWindow()
        {
            InitializeComponent();

            clients = FileWork.SetAllClientsList();
            StartState();
        }

        private void StartState()
        {
            clientsComboBox.ItemsSource = clients;
            clientsComboBox.DisplayMemberPath = "Name";
            clientsComboBox.SelectedValuePath = "Id";
            clientsComboBox.SelectedIndex = 0;

            transferComboBox.ItemsSource = clients;
            transferComboBox.DisplayMemberPath = "Name";
            transferComboBox.SelectedValuePath = "Id";
            transferComboBox.SelectedIndex = 1;

            accountsTypeComboBox.Items.Add("Недепозитный");
            accountsTypeComboBox.Items.Add("Депозитный");
            accountsTypeComboBox.SelectedIndex = 0;

            clientId = clientsComboBox.SelectedIndex;
            isDeposit = false;

            ShowCurrentClientInfo();
        }

        private void clientsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            clientId = clientsComboBox.SelectedIndex;
            ShowCurrentClientInfo();
        }

        private void accountsTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (accountsTypeComboBox.SelectedIndex == 0) isDeposit = false;
            else isDeposit = true;
            ShowCurrentClientInfo();
        }

        private void ShowCurrentClientInfo()
        {
            if (isDeposit)
            {
                accountNumberLabel.Content = $"Номер счета: {clients[clientId].DepositAccountClient.AccountNumber.ToString()}";
                string currentBalance = clients[clientId].DepositAccountClient.Balance.ToString("0.00");
                string newBalance = clients[clientId].CheckAccumulation(isDeposit).ToString("0.00");
                balanceLabel.Content = $"Баланс: {currentBalance} (через год: {newBalance})";
                string status = clients[clientId].DepositAccountClient.IsOpen ? "Открыт" : "Закрыт";
                statusLabel.Content = $"Статус счета: {status}";
                if (clients[clientId].DepositAccountClient.IsOpen) actionAccountButton.Content = "Закрыть счет";
                else actionAccountButton.Content = "Открыть счет";
                clientStatusLabel.Content = clients[clientId].GetType().Name;
            }
            else
            {
                accountNumberLabel.Content = $"Номер счета: {clients[clientId].AccountClient.AccountNumber.ToString()}";
                string currentBalance = clients[clientId].AccountClient.Balance.ToString("0.00");
                string newBalance = clients[clientId].CheckAccumulation(isDeposit).ToString("0.00");
                balanceLabel.Content = $"Баланс: {currentBalance} (через год: {newBalance})";
                string status = clients[clientId].AccountClient.IsOpen ? "Открыт" : "Закрыт";
                statusLabel.Content = $"Статус счета: {status}";
                if (clients[clientId].AccountClient.IsOpen) actionAccountButton.Content = "Закрыть счет";
                else actionAccountButton.Content = "Открыть счет";
                clientStatusLabel.Content = clients[clientId].GetType().Name;
            }
        }

        private void topUpButton_Click(object sender, RoutedEventArgs e)
        {
            clients[clientId].TopUpBalance(double.Parse(amountTextBox.Text), isDeposit);
            ShowCurrentClientInfo();
        }

        private void withdrawButton_Click(object sender, RoutedEventArgs e)
        {
            bool isSuccess = CheckAccounts(clientId);
            if (isSuccess)
            {
                clients[clientId].WithdrawBalance(double.Parse(amountTextBox.Text), isDeposit);
                ShowCurrentClientInfo();
            }
        }

        private void actionAccountButton_Click(object sender, RoutedEventArgs e)
        {
            if (isDeposit)
            {
                if (clients[clientId].DepositAccountClient.IsOpen) clients[clientId].CloseAccount(isDeposit);
                else clients[clientId].OpenAccount(isDeposit);
            }
            else
            {
                if (clients[clientId].AccountClient.IsOpen) clients[clientId].CloseAccount(isDeposit);
                else clients[clientId].OpenAccount(isDeposit);
            }
            ShowCurrentClientInfo();
        }

        private void transferButton_Click(object sender, RoutedEventArgs e)
        {
            int transferId = Convert.ToInt32(transferComboBox.SelectedValue);
            int fromId = Convert.ToInt32(clientsComboBox.SelectedValue);
            if (fromId != transferId)
            {
                bool isSuccess = CheckAccounts(transferId);
                if (isSuccess)
                {
                    clients[clientId].TransferBalance(clients[transferId], double.Parse(amountTextBox.Text), isDeposit);
                    ShowCurrentClientInfo();
                }
            }
            else MessageBox.Show("Выберите другого клиетна!");
        }

        private bool CheckAccounts(int transferId)
        {
            if (isDeposit)
            {
                if (!clients[transferId].DepositAccountClient.IsOpen || !clients[clientId].DepositAccountClient.IsOpen)
                {
                    MessageBox.Show("У клиента не открыт счет!");
                    return false;
                }
                else if (clients[clientId].DepositAccountClient.Balance < double.Parse(amountTextBox.Text))
                {
                    MessageBox.Show("У клиента недостаточно средств на счете!");
                    return false;
                }
            }
            else
            {
                if (!clients[transferId].AccountClient.IsOpen || !clients[clientId].AccountClient.IsOpen)
                {
                    MessageBox.Show("У клиента не открыт счет!");
                    return false;
                }
                else if (clients[clientId].AccountClient.Balance < double.Parse(amountTextBox.Text))
                {
                    MessageBox.Show("У клиента недостаточно средств на счете!");
                    return false;
                }
            }
            return true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            FileWork.SaveToFiles(clients);
        }
    }
}
